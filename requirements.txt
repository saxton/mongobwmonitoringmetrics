Complete output from command git rev-parse HEAD:
fatal: ambiguous argument 'HEAD': unknown revision or path not in the working tree.
Use '--' to separate paths from revisions, like this:
'git <command> [<revision>...] -- [<file>...]'
HEAD

----------------------------------------
alabaster==0.7.10
ansi2html==1.4.2
apipkg==1.4
appdirs==1.4.3
apptools==4.4.0
argcomplete==1.9.4
asn1crypto==0.24.0
astroid==1.6.0
astropy==2.0.1
astropy-helpers==2.0.1
atom==0.4.1
attrs==17.4.0
autopep8==1.3.3
Babel==2.5.3
backports==1.0
backports-abc==0.5
beautifulsoup4==4.6.0
biopandas==0.2.1
bitarray==0.8.1
bleach==1.5.0
blinker==1.4
blosc==1.5.1
bokeh==0.12.6
boto==2.48.0
Bottleneck==1.0.0
cairocffi==0.8.0
certifi==2018.4.16
cffi==1.11.4
chardet==3.0.4
click==6.7
cloudpickle==0.3.1
cntk==2.3.1
colorama==0.3.9
configobj==5.0.6
constantly==15.1.0
cov-core==1.15.0
coverage==4.2
cryptography==2.1.4
cvxopt==1.1.9
cycler==0.10.0
Cython==0.28.2
dask==2.0.0
decorator==4.2.1
docutils==0.14
entrypoints==0.2.3
enum34==1.1.6
envisage==4.6.0
et-xmlfile==1.0.1
ete3==3.0.0b35
execnet==1.5.0
file-magic==0.3.0
filelock==2.0.6
flake8==3.5.0
funcsigs==1.0.2
future==0.16.0
GDAL==2.2.2
gdbus-codegen==2.52.3
gevent==1.2.2
gmpy2==2.0.8
google-api-python-client==1.6.5
greenlet==0.4.12
gsd==1.2.0
gym==0.2.11
h5py==2.7.1
hgdistver==0.25
horovod==0.12.1
html5lib==0.9999999
httplib2==0.10.3
hyperlink==17.3.1
idna==2.6
imagesize==0.7.1
incremental==17.5.0
ipykernel==4.6.1
ipyparallel==6.0.2
ipython==5.4.1
ipython-genutils==0.2.0
ipywidgets==7.0.0
isort==4.3.4
jdcal==1.2
jedi==0.12.0
Jinja2==2.10
joblib==0.11
jplephem==2.6
jsonschema==2.6.0
jupyter-client==5.2.3
jupyter-console==5.1.0
jupyter-core==4.4.0
keyring==12.2.1
kiwisolver==1.0.1
lazy-object-proxy==1.3.1
line-profiler==2.1.2
linecache2==1.0.0
llvmlite==0.23.0
locket==0.2.0
lxml==4.2.0
Mako==1.0.6
Markdown==3.0.1
MarkupSafe==1.0
matplotlib==2.2.2
mayavi==4.5.0
mccabe==0.6.1
MDP==3.5
memory-profiler==0.47
meson==0.46.1
metakernel==0.20.4
metakernel-bash==0.11.3
metakernel-python==0.11.3
mistune==0.7.4
mock==2.0.0
mpmath==1.0.0
natsort==4.0.4
nbconvert==5.2.1
nbformat==4.4.0
ndg-httpsclient==0.4.2
netCDF4==1.2.4
netifaces==0.10.6
networkx==1.11
nose==1.3.7
nose2==0.6.5
notebook==5.5.0
numba==0.37.0
numexpr==2.6.2
numpy==1.14.2
numpydoc==0.7.0
oauth2client==4.1.2
objgraph==3.1.0
olefile==0.44
OpenMM==7.0.1
openpyxl==2.3.3
packaging==16.8
pandas==0.23.0
pandocfilters==1.4.2
parso==0.1.1
partd==0.3.8
path.py==10.3.1
pathlib2==2.3.0
patsy==0.4.1
pbr==4.0.3
pep8==1.7.0
pexpect==4.2.1
pickleshare==0.7.4
Pillow==4.3.0
pkgconfig==1.2.2
plac==0.9.6
plotly==1.9.6
ply==3.11
prompt-toolkit==1.0.15
protobuf==3.7.0
psutil==5.4.3
ptyprocess==0.5.2
py==1.5.3
pyamg==3.2.1
pyasn1==0.4.2
pyasn1-modules==0.2.1
pycairo==1.16.3
pycodestyle==2.3.1
pycparser==2.18
pycuda==2017.1
pycurl==7.43.0
pydot==1.2.3
pyelftools==0.24
pyephem==3.7.6.0
pyface==6.0.0
pyflakes==1.6.0
pyglet==1.2.4
Pygments==2.2.0
pygpu==0.6.7
## !! Could not determine repository location
pyhpcmongodb==1.0
pylibacl==0.5.0
pylint==1.8.2
pymongo==3.5.1
PyMySQL==0.8.0
pyopencl==2017.2
PyOpenGL==3.1.0
pyOpenSSL==17.5.0
pyparsing==2.2.0
pysam==0.12.0.1
PySocks==1.6.7
pytest-cache==1.0
python-dateutil==2.7.2
pytools==2017.4
pytz==2018.4
PyWavelets==0.5.2
pyxattr==0.6.0
PyYAML==3.12
pyzmq==17.0.0
qtconsole==4.3.1
queuelib==1.1.1
redis==2.10.6
regex==2017.4.5
reportlab==3.4.0
requests==2.18.4
rfc3987==1.3.7
rpy2==2.6.2
rsa==3.4.2
scandir==1.7
scikit-image==0.13.0
scikit-learn==0.19.0
scikit-sparse==0.3
scipy==1.0.0
seaborn==0.7.1
SecretStorage==2.3.1
Send2Trash==1.3.0
service-identity==17.0.0
setuptools-scm==1.16.1
sh==1.12.9
Shapely==1.5.17
simplegeneric==0.8.1
simplejson==3.14.0
six==1.11.0
snakefood==1.4
snowballstemmer==1.2.1
Sphinx==1.6.7
sphinx-rtd-theme==0.2.4
sphinxcontrib-websupport==1.0.1
SQLAlchemy==1.2.7
sqlalchemy-migrate==0.11.0
sqlparse==0.2.4
statsmodels==0.8.0
stevedore==1.28.0
strict-rfc3339==0.7
sympy==1.1.1
tables==3.4.2
Tempita==0.5.3.dev0
tensorflow==1.4.1
tensorflow-tensorboard==0.4.0
terminado==0.8.1
testpath==0.1
Theano==0.9.0
toolz==0.9.0
torch==0.3.0
torchvision==0.2.1
tornado==4.5.1
tqdm==4.31.1
traceback2==1.4.0
traitlets==4.3.2
traits==4.6.0
traitsui==6.0.0
trollius==2.1
txaio==2.9.0
typing==3.6.4
ujson==1.35
uritemplate==3.0.0
urllib3==1.22
vcversioner==2.16.0.0
versioneer==0.16
virtualenv==15.1.0
virtualenv-clone==0.2.6
virtualenvwrapper==4.8.2
wcwidth==0.1.7
webcolors==1.5
webencodings==0.5.1
Werkzeug==0.15.0
Whoosh==2.7.4
widgetsnbextension==3.0.0
Wraprun==0.2.1
wrapt==1.10.11
wslink==0.1.4
wxPython==4.0.2
xarray==0.9.6
xcffib==0.6.0
xlrd==1.0.0
xlwt==1.2.0
yt==3.4.0
zope.interface==4.4.3
