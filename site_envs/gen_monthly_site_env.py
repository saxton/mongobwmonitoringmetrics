import datetime
from datetime import datetime as dt

with open('mongo_sitevar_template.sh') as f:
    TEMPLATE_STR=f.read()

for year in range(2015, 2020):
    for month in range(1,13):
        with open('mongo_sitevar_{}{:0>2}.sh'.format(year, month), 'w') as f:
            _out_str = TEMPLATE_STR.replace('START_DATE="{START_DATE}"','START_DATE="{}{:0>2}01"'.format(year, month))
            _month_p1 = month+1
            _year = year
            if _month_p1 >12:
                _month_p1 = 1
                _year += 1
            _out_str = _out_str.replace('END_DATE="{END_DATE}"','END_DATE="{}{:0>2}01"'.format(_year, _month_p1))
            f.write(_out_str)
