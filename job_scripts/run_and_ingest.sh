#!/bin/bash
#PBS -l walltime=4:40:00
#PBS -N run_and_ingest
#PBS -l nodes=264:ppn=32:xe
#PBS -l flags=commtransparent
##PBS -l advres=saxton

module load bwpy
module load bwpy-mpi
source ~/Development/hpcmongodb/virenv/bin/activate

if [ -z "${MONGOCONFIGPATH}" ]
then

START_DATE="20180201"
END_DATE="20180301"
CHUNK_FACTOR=5

export CONFIG_SVRS_NUMBER_OF_INSTANCES=7
export ROUTER_SVRS_NUMBER_OF_INSTANCES=256
export ROUTER_SVRS_CONCURRENCY_PER_NODE=4
export SHARD_SVRS_NUMBER_OF_INSTANCES=256
export SHARD_SVRS_CONCURRENCY_PER_NODE=2

INIT_NUM_CHUNKS=$(( ${CHUNK_FACTOR} * ${SHARD_SVRS_NUMBER_OF_INSTANCES} ))
export MONGO_BIN=/opt/mongodb/4.0.5/bin
export MONGO_BASE_DIR=/u/staff/saxton/scratch/hpcmongodb/metric_store_${SHARD_SVRS_NUMBER_OF_INSTANCES}_shards_${START_DATE}_${END_DATE}
export MONGO_TMP=${MONGO_BASE_DIR}
export USE_MEMORY_AS_DISK=true
export INIT_EVAL_STR="sh.enableSharding(\"monitoringData\")
  sh.shardCollection( \"monitoringData.metricData\", { k_to_h : \"hashed\" }, false, { numInitialChunks: ${INIT_NUM_CHUNKS} } )
  sh.disableBalancing(\"monitoringData.metricData\")
  sh.shardCollection( \"monitoringData.torqueData\", { jobid : \"hashed\" }, false, { numInitialChunks: ${INIT_NUM_CHUNKS} } )
  sh.disableBalancing(\"monitoringData.torqueData\")
  monitoringData = db.getSiblingDB(\"monitoringData\")
  monitoringData.metricData.createIndex({CompId: 1})
  monitoringData.metricData.createIndex({\"#Time\": 1})
  monitoringData.torqueData.createIndex({dateTime: 1})
  sh.disableAutoSplit()
  config = db.getSiblingDB(\"config\")
  config.settings.save( { _id:\"chunksize\", value: 1024 } )"

SRC_DIR="/mnt/a/u/staff/saxton/Development/hpcmongodb" # "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
else
    source ${MONGOCONFIGPATH}
fi

export PROG_BOOT_LOCK_FILE=${SRC_DIR}/cluster_run/prog_boot_lock.lock

touch ${PROG_BOOT_LOCK_FILE}
cd ${SRC_DIR}/cluster_run

touch ${PROG_BOOT_LOCK_FILE}

./run.sh &
RUN_PID=$!

echo "sleep waitintg for cluster to become available"
while [  -f ${PROG_BOOT_LOCK_FILE} ]
do
  sleep 2
done

if [ -f $MONGO_BASE_DIR/mongo_force_shutdown.sem ]
then
    echo "Cluster Failed Startup, exiting"
    exit 246
else
    echo "Cluster is alive!"
fi

cd /mnt/a/u/staff/saxton/Development/mongobwmonitoringmetrics
echo "in dir $(pwd)"
cd pyhpcmongodb/ingest
module load bwpy
module load bwpy-mpi
H=$( cat ${MONGO_BASE_DIR}/router_svrs_and_ports.txt )
aprun -n 20 -N 2 -- python torque.py $H --dateRange ${START_DATE} ${END_DATE}
aprun -n 256 -N 4 -- python run.py $H --dateRange ${START_DATE} ${END_DATE} --metric_name ${MONGO_BASE_DIR}/ingest_${PBS_JOBID}

#sleep $(($PBS_WALLTIME-60*60))
kill -SIGUSR1 ${RUN_PID}

wait
