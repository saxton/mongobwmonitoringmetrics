#!/bin/bash -e
module load bwpy
module load bwpy-mpi

HPCMONGODB_SRC=${HOME}/Development/hpcmongodb
MONGOBWMONITORINGMETRICS_SRC=${HOME}/Development/mongobwmonitoringmetrics
source $HPCMONGODB_SRC/job_tools/run_multiple_clusters.sh
source $MONGOBWMONITORINGMETRICS_SRC/mongobwmonitoringmetrics-virenv/bin/activate

launch_clusters_and_ingest() {

LIST_OF_CONF_FILES=$@
launch_clusters $LIST_OF_CONF_FILES

export RUN_PID_LIST
export STARTER_HOSTS

echo "RUN_PID_LIST ${RUN_PID_LIST}"
echo "STARTER_HOSTS $STARTER_HOSTS"

for CONF_FILE in ${EX_LIST_OF_CONF_FILES}; do
    echo ${CONF_FILE}
    source $CONF_FILE
    aprun -n ${NUM_PE_TORQUE:-32} -N 4 -- python -m pyhpcmongodb.ingest.torque --dateRange $START_DATE $END_DATE $STARTER_HOSTS > ${MONGO_TMP}/logs/torque_${START_DATE}_${END_DATE}.log 2>&1 &
    INGEST_PID=$!
    INGEST_PID_LIST="$INGEST_PID $INGEST_PID_LIST"
done

for p in $INGEST_PID_LIST; do
    echo "Waiting on torque PID $p"
    wait $p
done

INGEST_PID_LIST=""

for CONF_FILE in ${EX_LIST_OF_CONF_FILES}; do
    echo ${CONF_FILE}
    source $CONF_FILE
    aprun -n ${NUM_PE_METRIC:-32} -N 4 -- python -m pyhpcmongodb.ingest.run --dateRange $START_DATE $END_DATE $STARTER_HOSTS > ${MONGO_TMP}/logs/metric_${START_DATE}_${END_DATE}.log 2>&1 &
    INGEST_PID=$!
    INGEST_PID_LIST="$INGEST_PID $INGEST_PID_LIST"
done

for p in $INGEST_PID_LIST; do
    echo "Waiting on ingest PID $p"
    wait $p
done

for p in $RUN_PID_LIST; do
    echo "Killing run.sh PID $p"
    kill $p
done

for p in $RUN_PID_LIST; do
    echo "Waiting on run.sh PID $p"
    wait $p
done
}
