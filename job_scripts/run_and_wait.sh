#!/bin/bash
#PBS -l walltime=6:00:00
#PBS -N run_and_wait
#PBS -l nodes=200:ppn=32:xe
#PBS -l flags=commtransparent
##PBS -l advres=saxton

module load bwpy
module load bwpy-mpi
echo "MONGOCONFIGPATH
 ${MONGOCONFIGPATH}"
if [ -z "${MONGOCONFIGPATH}" ]
then

CHUNK_FACTOR=5

export CONFIG_SVRS_NUMBER_OF_INSTANCES=7
export ROUTER_SVRS_NUMBER_OF_INSTANCES=256
export ROUTER_SVRS_CONCURRENCY_PER_NODE=4
export SHARD_SVRS_NUMBER_OF_INSTANCES=256
export SHARD_SVRS_CONCURRENCY_PER_NODE=2
#total 325 nodes
INIT_NUM_CHUNKS=$(( ${CHUNK_FACTOR} * ${SHARD_SVRS_NUMBER_OF_INSTANCES} ))
export MONGO_BIN=/opt/mongodb/4.0.5/bin
export MONGO_BASE_DIR=/u/staff/saxton/scratch/hpcmongodb/metric_store_256_shards_20180101_20180201
#export MONGO_BASE_DIR=/projects/monitoring_data/hpcmongo_monitoringData/bulk_store_${SHARD_SVRS_NUMBER_OF_INSTANCES}_shards_ramDisk
export MONGO_TMP=${MONGO_BASE_DIR}
export USE_MEMORY_AS_DISK=true
export READ_ONLY=true
export INIT_EVAL_STR="sh.enableSharding(\"monitoringData\")
  sh.shardCollection( \"monitoringData.metricData\", { k_to_h : \"hashed\" }, false, { numInitialChunks: ${INIT_NUM_CHUNKS} } )
  sh.disableBalancing(\"monitoringData.metricData\")
  sh.shardCollection( \"monitoringData.torqueData\", { jobid : \"hashed\" }, false, { numInitialChunks: ${INIT_NUM_CHUNKS} } )
  sh.disableBalancing(\"monitoringData.torqueData\")
  monitoringData = db.getSiblingDB(\"monitoringData\")
  monitoringData.metricData.createIndex({CompId: 1})
  monitoringData.metricData.createIndex({\"#Time\": 1})
  monitoringData.torqueData.createIndex({dateTime: 1})
  sh.disableAutoSplit()
  config = db.getSiblingDB(\"config\")
  config.settings.save( { _id:\"chunksize\", value: 2048 } )"

SRC_DIR="/mnt/a/u/staff/saxton/Development/hpcmongodb" # "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
else
    source ${MONGOCONFIGPATH}
fi

export PROG_BOOT_LOCK_FILE=${SRC_DIR}/cluster_run/prog_boot_lock.lock

touch ${PROG_BOOT_LOCK_FILE}
cd ${SRC_DIR}/cluster_run

touch ${PROG_BOOT_LOCK_FILE}

./run.sh &
RUN_PID=$!

echo "sleep waitintg for cluster to become available"
while [  -f ${PROG_BOOT_LOCK_FILE} ]
do
  sleep 2
done
echo "cluster is alive!"
cd ${SRC_DIR}

sleep $(($PBS_WALLTIME-60*60))

kill -SIGUSR1 ${RUN_PID}

wait
