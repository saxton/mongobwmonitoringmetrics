#!/bin/bash -e

MONGOBWMONITORINGMETRICS_SRC=${HOME}/Development/mongobwmonitoringmetrics

export NUM_PE_TORQUE=32
export NUM_PE_METRIC=256
source ${MONGOBWMONITORINGMETRICS_SRC}/job_scripts/run_multiple_clusters_ingest.sh

EX_LIST_OF_CONF_FILES="mongo_sitevar_201701.sh
mongo_sitevar_201702.sh
mongo_sitevar_201703.sh"

for F in $EX_LIST_OF_CONF_FILES; do
_EX_LIST_OF_CONF_FILES="$MONGOBWMONITORINGMETRICS_SRC/site_envs/$F $_EX_LIST_OF_CONF_FILES"
done
EX_LIST_OF_CONF_FILES=$_EX_LIST_OF_CONF_FILES

HPCMONGODB_SRC=${HOME}/Development/hpcmongodb
source $HPCMONGODB_SRC/job_tools/run_multiple_clusters.sh

#EX_LIST_OF_CONF_FILES="test.mongo_sitevar_201801.sh
#test.mongo_sitevar_201802.sh
#test.mongo_sitevar_201803.sh"

launch_clusters $EX_LIST_OF_CONF_FILES

echo "RUN_PID_LIST $RUN_PID_LIST"
echo "STARTER_HOSTS $STARTER_HOSTS"
