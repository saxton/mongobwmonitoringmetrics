CHUNK_FACTOR=5

START_DATE="20180301"
END_DATE="20180302"

export CONFIG_SVRS_NUMBER_OF_INSTANCES=2
export ROUTER_SVRS_NUMBER_OF_INSTANCES=8
export ROUTER_SVRS_CONCURRENCY_PER_NODE=4
export SHARD_SVRS_NUMBER_OF_INSTANCES=8
export SHARD_SVRS_CONCURRENCY_PER_NODE=2

INIT_NUM_CHUNKS=$(( ${CHUNK_FACTOR} * ${SHARD_SVRS_NUMBER_OF_INSTANCES} ))
export MONGO_BIN=/opt/mongodb/4.0.5/bin
export MONGO_BASE_DIR=/u/staff/saxton/scratch/hpcmongodb/test/metric_store_${SHARD_SVRS_NUMBER_OF_INSTANCES}_shards_${START_DATE}_${END_DATE}
export MONGO_TMP=${MONGO_BASE_DIR}
export USE_MEMORY_AS_DISK=true
export READ_ONLY=false #true
export INIT_EVAL_STR="sh.enableSharding(\"monitoringData\")
  sh.shardCollection( \"monitoringData.metricData\", { k_to_h : \"hashed\" }, false, { numInitialChunks: ${INIT_NUM_CHUNKS} } )
  sh.disableBalancing(\"monitoringData.metricData\")
  sh.shardCollection( \"monitoringData.torqueData\", { jobid : \"hashed\" }, false, { numInitialChunks: ${INIT_NUM_CHUNKS} } )
  sh.disableBalancing(\"monitoringData.torqueData\")
  monitoringData = db.getSiblingDB(\"monitoringData\")
  monitoringData.metricData.createIndex({CompId: 1})
  monitoringData.metricData.createIndex({\"#Time\": 1})
  monitoringData.torqueData.createIndex({dateTime: 1})
  sh.disableAutoSplit()
  config = db.getSiblingDB(\"config\")
  config.settings.save( { _id:\"chunksize\", value: 2048 } )"

SRC_DIR="/mnt/a/u/staff/saxton/Development/hpcmongodb" # "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
