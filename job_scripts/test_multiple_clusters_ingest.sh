#!/bin/bash -e
#PBS -l walltime=6:00:00
#PBS -N test_multiple_clusters_ingest
#PBS -l nodes=48:ppn=32:xe
#PBS -l flags=commtransparent
##PBS -l advres=saxton

export NUM_PE_TORQUE=32
export NUM_PE_METRIC=32

source run_multiple_clusters_ingest.sh

EX_LIST_OF_CONF_FILES="test.mongo_sitevar_201801.sh
test.mongo_sitevar_201802.sh
test.mongo_sitevar_201803.sh"

launch_clusters_and_ingest $EX_LIST_OF_CONF_FILES
