from setuptools import setup
from setuptools import find_packages

setup(name='pyhpcmongodb',
      version='1.0',
      description='Python Package to ingest and extract monitoring data from a mongodb cluster',
      author='Aaron Saxton',
      author_email='saxton@illinois.edu',
      url='',
      packages=find_packages(),
      install_requires=[
        ]
     )
