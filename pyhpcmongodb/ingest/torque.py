try:
    from mpi4py import MPI
    import mpi4py
except ImportError:
    has_mpi = False
else:
    has_mpi = True
    comm = MPI.COMM_WORLD
    nprocs = comm.Get_size()
    if nprocs == 1:
        has_mpi = False

if not has_mpi:
    print('No MPI')

import os
import argparse
from pymongo import MongoClient
from pymongo.errors import BulkWriteError
import datetime
from datetime import datetime as dt
import random
from pyhpcmongodb.ingest.run import get_hosts_ports_of_cluster_from_conf_filename

torq_dir = "/u/staff/saxton/torque_data/data/bw/torque"
torq_file_name_template="TEMP-{}"
torq_dir = "/u/staff/saxton/torque_data/data/var"
torq_file_name_template="{}"
database_name = 'monitoringData'

def does_file_exist(f_name):
    try:
        os.stat(f_name)
    except FileNotFoundError:
        return False
    else:
        return True

def gen_date_list_str(D1, D2):
    numdays = (D2 - D1).days
    date_list = [D1 + datetime.timedelta(days=x) for x in range(0, numdays)]
    date_list_str = [d.strftime("%Y%m%d") for d in date_list]
    return date_list_str

def ingest_torque(D1, D2, hosts):
    date_list = gen_date_list_str(D1, D2)
    if has_mpi:
        num_chunks = max(int(len(date_list)/nprocs), 1)
        wip = [date_list[i:i + num_chunks] for i in range(0, len(date_list), num_chunks)]
        if comm.rank < len(wip):
            date_list = wip[comm.rank]
        else:
            return
    host = random.choice(hosts)
    print("D1: {}, D2: {}, date_list: {}".format(D1, D2, date_list))
    client = MongoClient(host, 27019, username="admin", password="admin")
    #client = MongoClient(host, 27019)
    db = client.get_database(database_name)
    torqueData = db.torqueData

    for f_name in date_list:
        f_name_path = os.path.join(torq_dir, torq_file_name_template.format(f_name))
        print('f_name {}'.format(f_name_path))
        if does_file_exist(f_name_path):
            pass
        else:
            print('f_name {} does not exist, skipping'.format(f_name_path))
            continue

        with open(f_name_path) as f:
            d = [l.strip() for l in f]

        doc_keys = ["dateTime", 'action', 'jobid', 'data']

        _records = [{k:v for k,v in zip(doc_keys, l.split(';')) } for l in d]
        records = []
        for record in _records:
            record['dateTime'] = dt.strptime(record['dateTime'], "%m/%d/%Y %H:%M:%S")
            if record['action'] != 'A':
                wip = record['data'].split(' ')
                record['data']=dict(w.split('=',1) if len(w.split('=', 1)) > 1 else (w, None)  for w in wip)
                keys = list(record['data'].keys())
                for k in keys:
                    record['data'][k.replace('.', '-')] = record['data'].pop(k)
                records.append(record)
            else:
                records.append(record)

        resp = torqueData.insert_many(records)
        print('resp: {}'.format(resp))


if __name__== "__main__":
    def valid_date(s):
        try:
            return dt.strptime(s, "%Y%m%d")
        except ValueError:
            msg = "Not a valid date: '{0}'.".format(s)
            raise argparse.ArgumentTypeError(msg)

    parser = argparse.ArgumentParser(description='ingest config args')

    parser.add_argument('hosts_ports', type=str, nargs='+',
                        help='router host')
    parser.add_argument('--dateRange', type=valid_date, nargs=2,
                        help='dates in format YYYYMMDD (e.g. 20181119). '
                        'First date must be smaller that second. '
                        'Dates will be denerated in as the half '
                        'open interval, e.g. [0,1). ')

    args = parser.parse_args()
    start_date, end_date = args.dateRange[0], args.dateRange[1]
    starter_hosts = list(set([h.split(':')[0] for h in args.hosts_ports]))
    hosts = get_hosts_ports_of_cluster_from_conf_filename(starter_hosts, start_date, end_date, rank=comm.rank)
    print('hosts {}'.format(hosts))
    ingest_torque(start_date, end_date, hosts)
