#date --date '01/01/2018 03:00:00' +"%s" #to get epoch time
try:
    from mpi4py import MPI
    import mpi4py
except ImportError:
    has_mpi = False
else:
    has_mpi = True
    comm = MPI.COMM_WORLD
    nprocs = comm.Get_size()
    if nprocs == 1:
        has_mpi = False

if not has_mpi:
    print('No MPI')

import sys
import math
from pymongo import MongoClient
from pymongo.errors import BulkWriteError
from pyhpcmongodb.query.get import get_constelation_hosts
from datetime import datetime as dt
import datetime
import numpy as np
import argparse
import os
import time
import random
import os
import re
from os.path import join as ospJoin
monitoring_data_path = "/projects/monitoring_data/ovis"
monitoring_data_path = "/mnt/b/projects/sciteam/gjo/share/node_metrics/cray_system_sampler"
#monitoring_data_path = "/mnt/a/u/staff/saxton/Development/hpcmongodb/ingest/moc_data"
f_name_header = 'HEADER.20160115-'
database_name = 'monitoringData'
#metric_file_name = "metric_full_job_ir{ir}_rh{rh}.txt"
metric_file_name_template = "{name}_ir{ir}_rh{rh}.txt"
metric_report = "rank: {rank}, c_time: {c_time}, lines: {lines}, delta_t: {delta_t}, file: {file}\n"

DOC_BLOCK_SIZE=128
DOC_BLOCK_PROC_SIZE=1000000*1500 #1500 is average line length
DOC_BLOCK_PROC_SIZE=100000*1500 #1500 is average line length
DOC_BLOCK_PROC_SIZE=20000*1500 #1500 is average line length
TEST=False



if has_mpi:
    print("has_mpi, nprocs {}, rank {}".format(nprocs, comm.rank))
    print_move_line = nprocs - comm.rank + 1
    status_str = ''.join(["\033[F"]*print_move_line + [str(comm.rank)] + [' {}>{}>line_count {}, %{}, lines/sec {:.3f}, file {}, offset {}, blkErrCount {}'] + ['\n']*print_move_line + ['\r'])
    found_eof_str = ''.join(["\033[F"]*print_move_line + [str(comm.rank)] + [' {}>Found EOF, file {}, offset {}'] + ['\n']*print_move_line + ['\r'])
    found_eof_wait_str = ''.join(["\033[F"]*print_move_line + [str(comm.rank)] + [' {}>Found EOF, waiting, file {}, offset {}'] + ['\n']*print_move_line + ['\r'])
    at_barr_str = ''.join(["\033[F"]*print_move_line + [str(comm.rank)] + ['>At Barrier'] + ['\n']*print_move_line + ['\r'])
    no_top_status_str = ' {}>{}>line_count {}, %{}, lines/sec {:.3f}, file {}, offset {}, blkErrCount {}'
    no_top_found_eof_str = ' {}>Found EOF, file {}, offset {}'
    no_top_found_eof_wait_str = ' {}>Found EOF, waiting, file {}, offset {}'
    no_top_at_barr_str = ''.join([str(comm.rank)] + ['>At Barrier'])

else:
    status_str = '>line_count {}, lines/sec {:.3f}, file {}, offset {}, blkErrCount {}\r'

class backOffException(Exception):
    pass

def get_hosts_ports_of_cluster_from_conf_filename(starter_hosts, start_date, end_date, rank=0):
    num_sh = len(starter_hosts)
    s_host = starter_hosts[rank%num_sh]
    
    constelation_hosts = get_constelation_hosts(s_host)
    for conf_name in constelation_hosts:
        match = re.search(r'\d{4}\d{2}', conf_name)
        date = dt.strptime(match.group(), '%Y%m').date()
        print('date: {}, start_date {}'.format(date, start_date))
        if date == start_date.date():
            return [':'.join([h['host'], str(int(h['port']))]) for h in constelation_hosts[conf_name]]

def backOffGen(init, rate, maxStep):
    '''
    init*e^(rate*step)
    '''
    for step in range(maxStep):
        yield init*math.exp(rate*step)
    raise backOffException("Querry Failed too many times")
    
def does_file_exist(f_name):
    try:
        os.stat(f_name)
    except FileNotFoundError:
        return False
    else:
        return True
    
def load_block_from_file(f_path_name, db_host_name, db_host_port, f_blk_size=None, f_blk_offset=None, top=False ):
    client = MongoClient(db_host_name, db_host_port, username="admin", password="admin")
    db = client.get_database(database_name)
    metricData = db.metricData

    with open(ospJoin(monitoring_data_path, f_name_header), 'r') as f:
        h_str = f.read()

    f_tot_size = os.stat(f_path_name).st_size
    h_str_list = [l.strip().replace('.','_') for l in h_str.split(',')]
    h_type_list = [float] + [int]*len(h_str_list)
    failed_insert_many = []
    tick = time.time()
    with open(f_path_name, 'r') as f:
        if f_blk_offset is not None:
            f.seek(f_blk_offset)
            c = f.read(1)
            while c != '\n':
                if c == '':
                    EOF = True
                    client.close()
                    return EOF, 0
                c = f.read(1)
            
        metricData_list = []
        resp=None
        lines_p_sec = None
        num_char_tot = 0


        for line_count, l in enumerate(f):
            num_char_tot += len(l)
            if TEST and int(line_count / DOC_BLOCK_SIZE) == 3:
                client.close()
                return EOF, line_count

            if l == '':
                EOF = True
                client.close()
                return EOF, line_count

            split_str = l.split(',')
            concat_t_id=split_str[0]+split_str[2]
            try:
                doc = {k: t(v.strip()) for k, t, v in zip(h_str_list, h_type_list, split_str)}
            except ValueError as e:
                print("Handeling ValueError, skipping doc in file {}. Details {}".format(f_path_name,
                                                                                         e.with_traceback(sys.exc_info()[2])))
            else:
                doc['k_to_h'] = concat_t_id
                doc['#TimeTrunSec'] = int(doc['#Time'])
                metricData_list.append(doc)
            if (line_count % DOC_BLOCK_SIZE == 0 and line_count != 0) or DOC_BLOCK_SIZE == 1:
                if DOC_BLOCK_SIZE == 1:
                    resp = metricData.insert_one(metricData_list[0])
                else:
                    try:
                        for back_off_time in backOffGen(init=.5, rate=.5, maxStep=100):
                            try:
                                resp = metricData.insert_many(metricData_list, ordered=False)
                            except BulkWriteError as bwe:
                                failed_indecies = [i['index'] for i in bwe.details['writeErrors']]
                                metricData_list = [metricData_list[i] for i in failed_indecies]
                                client.close()
                                print('handeling write error, backoff. Sleeping {} sec'.format(back_off_time))
                                time.sleep(back_off_time)
                            else:
                                break
                    except backOffException:
                        if top:
                            pass
                        else:
                            load_block_from_file.BulkErrorCounter+=1
                            print('rank {}> BulkWriteError. '
                                  'line_count: {} '
                                  'f_blk_offset: {} '
                                  'f_path_name: {}'.format(
                                    comm.rank if has_mpi else 'Nan',
                                    line_count, f_blk_offset, f_path_name
                                    )
                                  )


                metricData_list = []

                tock = time.time()
                lines_p_sec = DOC_BLOCK_SIZE/float(tock-tick)
                tick = time.time()
                pct_done = int(100.*(f_blk_offset+num_char_tot)/float(f_tot_size))
                if resp is not None:
                    if top:
                        print(status_str.format(db_host_name, comm.rank, line_count,
                                                pct_done, lines_p_sec,
                                                f_path_name.split('/')[-1], f_blk_offset,
                                                load_block_from_file.BulkErrorCounter), end='')
                    elif line_count % (DOC_BLOCK_SIZE*16) == 0:
                        print(no_top_status_str.format(db_host_name, comm.rank, line_count,
                                                       pct_done, lines_p_sec,
                                                       f_path_name.split('/')[-1], f_blk_offset,
                                                       load_block_from_file.BulkErrorCounter))
                        
#                    f_metric.write("{},{},{}\n".format(comm.rank, lines_p_sec, line_count))
#                else:
#                    print(status_str.format(db_host_name, line_count,
#                                            -1, 0.0,
#                                            f_path_name.split('/')[-1], f_blk_offset), end='')
            if num_char_tot >= DOC_BLOCK_PROC_SIZE:
                EOF = False
                client.close()
                return EOF, line_count
    EOF = True
    line_count = 0
    client.close()
    return EOF, line_count

load_block_from_file.BulkErrorCounter=0

def gen_date_list_str(D1, D2):
    numdays = (D2 - D1).days
    date_list = [D1 + datetime.timedelta(days=x) for x in range(0, numdays)]
    date_list_str = [d.strftime("%Y%m%d") for d in date_list]
    return date_list_str

if __name__== "__main__":
    def valid_date(s):
        try:
            return dt.strptime(s, "%Y%m%d")
        except ValueError:
            msg = "Not a valid date: '{0}'.".format(s)
            raise argparse.ArgumentTypeError(msg)

    parser = argparse.ArgumentParser(description='ingest config args')

    parser.add_argument('starter_hosts', type=str, nargs='+',
                        help='list of router starter hosts')
    parser.add_argument('--dateRange', type=valid_date, nargs=2,
                        help='dates in format YYYYMMDD (e.g. 20181119). '
                        'First date must be smaller that second. '
                        'Dates will be denerated in as the half '
                        'open interval, e.g. [0,1). ')
    parser.add_argument('--metric_name', type=str,
                        help='the name to prepend the metrics file. This can include a full path.')
    parser.add_argument('--top', action='store_true', default=False,
                        help='print ingest pregress interactivly')
    args = parser.parse_args()

    data_list_str = gen_date_list_str(args.dateRange[0], args.dateRange[1])
    # need to check that all these files exists ^^^^
    candidate_metric_files = [ospJoin(monitoring_data_path, d) for d in data_list_str]
    _cmf_mask = [does_file_exist(cf) for cf in candidate_metric_files]
    data_list_str = [d for m, d in zip(_cmf_mask, data_list_str) if m ]
    num_metric_files = len(data_list_str)
    if num_metric_files == 0:
        raise Exception("No metric files found at {}. Exiting".format(monitoring_data_path))
    hosts = get_hosts_ports_of_cluster_from_conf_filename(args.starter_hosts,
                                                          args.dateRange[0],
                                                          args.dateRange[1],
                                                          rank=comm.rank)
    print('hosts {}'.format(hosts))
    num_hosts = len(hosts)
    
    if has_mpi:
        do_data = [0,0] #np.array([0,0], dtype='i')
        finish_data = [0,0,0,0] #np.array([0,0], dtype='i')
        if comm.rank == 0:
            #print(''.join(['\n']*(print_move_line+1)+['\r']), end='')
            progress_table = {k: {"offset" : 0, "index": i} for i, k in enumerate(data_list_str)}
            comm.barrier()
            #initial dispatch
            for i in range(1, nprocs):
                file_index = (i -1 ) % num_metric_files
                do_data[0] = file_index
                do_data[1] = progress_table[data_list_str[file_index]]["offset"]
                resp = comm.isend(do_data, dest=i, tag=11)
                resp.wait()
                progress_table[data_list_str[file_index]]["offset"] += DOC_BLOCK_PROC_SIZE
            sc = 0
            req_str = ''
            reqs = [comm.irecv(source=i, tag=11) for i in range(1,nprocs)]
            if args.top:
                pass
            else:
                print('starting on rank 0, total ranks {}'.format(nprocs))
            with open(metric_file_name_template.format(name=args.metric_name,ir=nprocs-1,rh=num_hosts), 'w') as f_metric:
                while progress_table:
                    #time.sleep(2)
                    if args.top:
                        print("{}> sleep count {}, files left {}, req {}\r".format(comm.rank,
                                                                                   sc, len(progress_table),
                                                                                   req_str), end='')
                    sc+=1
                    for i in range(1, nprocs):
                        req = reqs[i-1].test()
                        if req[0] == True:
                            req_str="True Step {}, req {}".format(sc, req)
                            finish_data = req[1]
                            f_index = finish_data[1]
                            if finish_data[0] == 1:
                                if data_list_str[f_index] in progress_table.keys():
                                    del progress_table[data_list_str[f_index]]
                            if len(progress_table.keys()) == 0:
                                break
                            next_f = random.choice(list(progress_table.keys()))
                            do_data[0] = progress_table[next_f]["index"]
                            do_data[1] = progress_table[next_f]["offset"]
                            comm.isend(do_data, dest=i, tag=11)
                            progress_table[next_f]["offset"] += DOC_BLOCK_PROC_SIZE
                            reqs[i-1] = comm.irecv(source=i, tag=11)
                            m_dat = {
                                'rank': i,
                                'file': data_list_str[f_index],
                                'lines': finish_data[2],
                                'delta_t': finish_data[3],
                                'c_time': time.time()
                                }
                            f_metric.write(metric_report.format(**m_dat))
                            f_metric.flush()
                        else:
                            pass
                    time.sleep(1)
                do_data[0] = -1
                time.sleep(1)
                for i in range(1, nprocs):
                    if args.top:
                        print("{}> sending done to {}\r".format(comm.rank, i), end='')
                    else:
                        print("{}> sending done to {}".format(comm.rank, i))
                    resp = comm.isend(do_data, dest=i, tag=11)
                    resp.wait()
                time.sleep(1)
        else:
            time.sleep(random.uniform(0,2))
            mongodb_host_name_port = hosts[comm.rank % num_hosts]
            mongodb_host_name, mongodb_host_port = mongodb_host_name_port.split(':')
            mongodb_host_port=int(mongodb_host_port)
            EOF=False
            comm.barrier()
            tick = time.time()
            while True:
                resp = comm.irecv(source=0, tag=11)
                if EOF:
                    if args.top:
                        print(found_eof_wait_str.format(mongodb_host_name_port, m_file_path.split('/')[-1], do_data[1]), end='')
                    else:
                        print(no_top_found_eof_wait_str.format(mongodb_host_name_port, m_file_path.split('/')[-1], do_data[1]))
                do_data = resp.wait()
                if do_data[0] == -1:
                    break
                m_file_name = data_list_str[do_data[0]]
                m_file_path = ospJoin(monitoring_data_path, m_file_name)
                try:
                    EOF, line_count = load_block_from_file(m_file_path,
                                                           mongodb_host_name,
                                                           mongodb_host_port,
                                                           f_blk_size=DOC_BLOCK_PROC_SIZE,
                                                           f_blk_offset=do_data[1],
                                                           top=args.top)
                except Exception as e:
                    print("Handeling Exception From load_block_from_file, skipping block in file {} at {}. Details {}".format(m_file_path, do_data[1],
                                                                                                                              e.with_traceback(sys.exc_info()[2])))
                    EOF=False
                    line_count=0

                tock = time.time()
                if EOF:
                    finish_data[0] = 1
                    finish_data[1] = do_data[0]
                    finish_data[2] = line_count
                    finish_data[3] = tock-tick
                    if args.top:
                        print(found_eof_str.format(mongodb_host_name_port, m_file_path.split('/')[-1], do_data[1]), end='')
                    else:
                        print(no_top_found_eof_str.format(mongodb_host_name_port, m_file_path.split('/')[-1], do_data[1]))
                    resp = comm.isend(finish_data, dest=0, tag=11)
                    resp.wait()
                else:
                    finish_data[0] = 0
                    finish_data[1] = do_data[0]
                    finish_data[2] = line_count
                    finish_data[3] = tock-tick
                    resp = comm.isend(finish_data, dest=0, tag=11)
                    resp.wait()
                tick = time.time()
                time.sleep(1)
        if args.top:
            print(at_barr_str, end='')
        else:
            print(no_top_at_barr_str)
        comm.barrier()
    else:
        mongodb_host_name_port = hosts[0]
        mongodb_host_name, mongodb_host_port = mongodb_host_name_port.split(':')
        mongodb_host_port=int(mongodb_host_port)
        
        f_name_date_str = args.dateRange[0].strftime("%Y%m%d")
        load_block_from_file(
            ospJoin(monitoring_data_path, f_name_date_str),
            mongodb_host_name,
            mongodb_host_port
            )
        

#scrap
#for i in $(echo $(for i in $(seq 1 30); do du -sh data-${i}; done) | grep -Po '(\d+)G' | tr -d 'G'); do let A=A+${i}; done
#for k in  $(for i in $(seq 1 30); do du -sk data-${i} | cut -d 'd' -f 1; done); do let A=A+${k}; done
