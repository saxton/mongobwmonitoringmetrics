import argparse

first_x_sec = 1800
last_x_sec = 1800

if __name__== "__main__":
    parser = argparse.ArgumentParser(description='ingest config args')
        
    parser.add_argument('file_name', type=str,
                        help='generate metric report')


    args = parser.parse_args()

    with open(args.file_name) as f:
        d_raw = [l for l in f]

    field_type=[int, float, int, float, lambda x: str(x).strip()]

    dat = [
        {
            kv.split(':')[0].strip(): t(kv.split(':')[1])
            for kv,t in zip(d.split(','), field_type)
            }
        for d in d_raw
        ]

    start = min([l['c_time'] for l in dat])
    end = max([l['c_time'] for l in dat])
    tot_lines = sum([l['lines'] for l in dat])

    tot_line_sec = tot_lines/(end - start)
    print('tot_line_sec: {}'.format(tot_line_sec))

    end_limit = start+first_x_sec

    tot_lines = sum([l['lines'] for l in dat if l['c_time'] < end_limit])
    tot_time = max([l['c_time'] for l in dat if l['c_time'] < end_limit])
    tot_line_sec = tot_lines/(tot_time - start)
    print('first_x_sec: {}, tot_line_sec: {}'.format(first_x_sec, tot_line_sec))

    start_limit = end-last_x_sec

    tot_lines = sum([l['lines'] for l in dat if l['c_time'] > start_limit])
    tot_time = min([l['c_time'] for l in dat if l['c_time'] > start_limit])
    tot_line_sec = tot_lines/(end - tot_time)
    print('last_x_sec: {}, tot_line_sec: {}'.format(last_x_sec, tot_line_sec))
