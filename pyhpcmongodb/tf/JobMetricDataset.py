from pyhpcmongodb.query.get import get_constelation_hosts
from pyhpcmongodb.query.get import make_job_cube
from pyhpcmongodb.query.get import jobs_strata_metrics
from pyhpcmongodb.query.get import jobs_from_group_metrics
from pyhpcmongodb.query.get import get_job_metrics
from pyhpcmongodb.query.get import EXPECTED_NUM_KEY_METRICS
from pyhpcmongodb.query.get import DATA_CUBE_DIM
from sys import getsizeof
import tensorflow as tf
import numpy as np
import itertools
import time

def chunk_up(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def build_strata_dataset(start_d, end_d,
                         min_num_nodes, max_num_nodes,
                         min_duration, max_duration,
                         limit, index=0, num_workers=1, data_cube_t_length=60, offset=None, shuffle=False,
                         hosts_ports=None, dbs=None):
    delta_step=data_cube_t_length*60
    def job_metric_gen():
        jobs_strata_gen = jobs_strata_metrics(start_d, end_d,
                                              min_num_nodes, max_num_nodes,
                                              min_duration, max_duration,
                                              limit, index=index, num_workers=num_workers,
                                              shuffle=shuffle, hosts_ports=hosts_ports,
                                              dbs=dbs, delta_step=delta_step)
        zero_cube_chunk = [make_job_cube(None, data_cube_t_length=data_cube_t_length,
                                         key_length=EXPECTED_NUM_KEY_METRICS)]
        prev_jobid = None
        chunk_i=0
        for jobid, jm, t_min, t_max in jobs_strata_gen:
            if prev_jobid != jobid:
                yield [-1], zero_cube_chunk, ['noJob'.encode('utf-8')]
                prev_jobid = jobid
            tick = time.time()
            try:
                d_cube = make_job_cube(jm, data_cube_t_length=data_cube_t_length, jobid=jobid,
                                       t_min=t_min, t_max=t_max, normalize=True)
            except StopIteration:
                print('jobid {} has zero length, skipping'.format(jobid))
                continue
            tock = time.time()

            print("rank {}> jobid {}, make_job_cube wall clock time (sec): {:.3f}".format(index, jobid, tock-tick))
            d_cube_chunked = chunk_up(d_cube, data_cube_t_length)
            #fillvalue=zero_cube_batch)
            # the following is what we are shooting for
            # list(itertools.zip_longest(list(chunk_up(np.arange(10), 3)), list(chunk_up(np.arange(17), 3))))
            for chunk_i, batch_chunk in enumerate(d_cube_chunked):
                yield [chunk_i], [batch_chunk], [jobid.encode('utf-8')]

        yield [-1], zero_cube_chunk, ["noJob".encode('utf-8')]

    data_shape = [1, data_cube_t_length] + 3*[DATA_CUBE_DIM] + [EXPECTED_NUM_KEY_METRICS]
    # data_shape = [None]
    dataset = tf.data.Dataset.from_generator(
        job_metric_gen,
        (tf.int32, tf.float32, tf.string),
        (tf.TensorShape([1]),
         tf.TensorShape(data_shape),
         tf.TensorShape([1]))
    )
    dataset = dataset.prefetch(4)
    return dataset

def build_dataset_from_group(group, limit, index=0,
                             num_workers=1, data_cube_t_length=60,
                             offset=None, shuffle=False,
                             hosts_ports=None, dbs=None):
    delta_step=data_cube_t_length*60
    def job_metric_gen():
        jobs_from_group_gen = jobs_from_group_metrics(group, delta_step, hosts_ports, dbs, index, num_workers, limit, offset)

        zero_cube_chunk = [make_job_cube(None, data_cube_t_length=data_cube_t_length,
                                         key_length=EXPECTED_NUM_KEY_METRICS)]
        prev_jobid = None
        chunk_i=0
        for jobid, jm, t_min, t_max in jobs_from_group_gen:
            print("getting jobid {} metadata. t_min {}, t_max {}".format(jobid, t_min, t_max))
            if prev_jobid != jobid:
                yield [-1], zero_cube_chunk, ['noJob'.encode('utf-8')]
                prev_jobid = jobid
            tick = time.time()
            try:
                d_cube = make_job_cube(jm, data_cube_t_length=data_cube_t_length, jobid=jobid,
                                       t_min=t_min, t_max=t_max, normalize=True)
            except StopIteration:
                print('jobid {} has zero length, skipping'.format(jobid))
                continue
            tock = time.time()

            print("rank {}> jobid {}, make_job_cube wall clock time (sec): {:.3f}".format(index, jobid, tock-tick))
            d_cube_chunked = chunk_up(d_cube, data_cube_t_length)
            #fillvalue=zero_cube_batch)
            # the following is what we are shooting for
            # list(itertools.zip_longest(list(chunk_up(np.arange(10), 3)), list(chunk_up(np.arange(17), 3))))
            for chunk_i, batch_chunk in enumerate(d_cube_chunked):
                yield [chunk_i], [batch_chunk], [jobid.encode('utf-8')]

        yield [-1], zero_cube_chunk, ["noJob".encode('utf-8')]

    data_shape = [1, data_cube_t_length] + 3*[DATA_CUBE_DIM] + [EXPECTED_NUM_KEY_METRICS]
    # data_shape = [None]
    dataset = tf.data.Dataset.from_generator(
        job_metric_gen,
        (tf.int32, tf.float32, tf.string),
        (tf.TensorShape([1]),
         tf.TensorShape(data_shape),
         tf.TensorShape([1]))
    )
    dataset = dataset.prefetch(3)
    return dataset


def build_dataset_from_jobid_list(job_ids, host=None, db=None):

    def job_metric_gen():
        zero_cube_chunk = [make_job_cube(None, data_cube_t_length=data_cube_t_length,
                                         key_length=EXPECTED_NUM_KEY_METRICS)]
        yield [-1], zero_cube_chunk, ['noJob'.encode('utf-8')]
        prev_jobid = None
        chunk_i=0
        for job_id in job_ids:
            jm, _ = get_job_metrics(jobid, host=host, db=db)
            d_cube = make_job_cube(jm, data_cube_t_length=data_cube_t_length)

            d_cube_chunked = chunk_up(d_cube, data_cube_t_length)

            #fillvalue=zero_cube_batch)
            # the following is what we are shooting for
            # list(itertools.zip_longest(list(chunk_up(np.arange(10), 3)), list(chunk_up(np.arange(17), 3))))
            for chunk_i, batch_chunk in enumerate(d_cube_chunked):
                yield [chunk_i], [batch_chunk], [job_id.encode('utf-8')]
            yield [-1], zero_cube_chunk, ["noJob".encode('utf-8')]
        
    data_shape = [1, data_cube_t_length] + 3*[DATA_CUBE_DIM] + [EXPECTED_NUM_KEY_METRICS]    
    dataset = tf.data.Dataset.from_generator(job_metric_gen, (tf.float32), tf.TensorShape(data_shape))

    return dataset
