import argparse
from datetime import datetime as dt
from pymongo import MongoClient
from tqdm import tqdm 
database_name = 'monitoringData'

def count_docs_at_time(start, end, host=None, db=None):
    if host is not None:
        client = MongoClient(host, 27019, username="admin", password="admin")
        db = client.get_database(database_name)
        metricData = db.metricData
    elif db is not None:
        metricData = db.metricData
    else:
        raise Exception('Must pass host or db.')
    time_slice_counts = []
    for time_slice in range(start, end, 60):
        c = metricData.find({"#Time": {'$gt': time_slice - 0.5, '$lt': time_slice + 0.5}}).count()
        print('time_slice_counts: {}'.format(c))
        time_slice_counts.append(c)

    return time_slice_counts

def rm_metricData_duplicates(start, end, host=None, db=None):
    REMOVE_CHUNK_SIZE = 100
    if host is not None:
        client = MongoClient(host, 27019, username="admin", password="admin")
        db = client.get_database(database_name)
        metricData = db.metricData
    elif db is not None:
        metricData = db.metricData
    else:
        raise Exception('Must pass host or db.')

    cursor = metricData.aggregate(
        [
            {"$match": {"#Time": { "$gte": start, "$lt": end }}},
            {"$group": {"_id": "$k_to_h", "unique_ids": {"$addToSet": "$_id"}, "count": {"$sum": 1}}},
            {"$match": {"count": { "$gte": 2 }}}
        ]
    )

    response = []
    for doc in tqdm(cursor, desc="doc cursor"):
        #print('{} has {} Duplicate, Staging Drop'.format(doc["unique_ids"][0], len(doc["unique_ids"])))
        del doc["unique_ids"][0]
        for id in tqdm(doc["unique_ids"], desc="duplicate docs", leave=False):
            response.append(id)

    print("Commiting to remove {} docs".format(len(response)))
    metricData.remove({"_id": {"$in": response}})


if __name__== "__main__":
    def valid_date(s):
        try:
            return dt.strptime(s, "%Y%m%dT%H:%M")
        except ValueError:
            msg = "Not a valid date: '{0}'.".format(s)
            raise argparse.ArgumentTypeError(msg)

    parser = argparse.ArgumentParser(description="Some Validation Tools")
    global_parser = argparse.ArgumentParser(add_help=False)

    global_parser.add_argument('host', type=str,
                               help='Router host (nidxxxxxx)')
    
    global_parser.add_argument('--dateRange', type=valid_date, nargs=2,
                              help='dates in format YYYYMMDDTHH:mm (e.g. 20181119T14:30). '
                              'First date must be smaller that second. '
                              'Dates will be denerated in as the half '
                              'open interval, e.g. [0,1). ')
    
    sp = parser.add_subparsers()
    count_parser = sp.add_parser('count', parents=[global_parser],
                                 help='Counts number of documents at each minute time '
                                 'slice in dateRange')
    dropDup_parser = sp.add_parser('dropDup', parents=[global_parser], help="drops duplicates")


    def do_count(args):
        start = int(args.dateRange[0].timestamp())
        end = int(args.dateRange[1].timestamp())

        time_slice_counts = count_docs_at_time(start, end,host=args.host)
        
    def do_dropDup(args):
        start = int(args.dateRange[0].timestamp())
        end = int(args.dateRange[1].timestamp())
        rm_metricData_duplicates(start, end, host=args.host)


    count_parser.set_defaults(func=do_count)
    dropDup_parser.set_defaults(func=do_dropDup)
    args = parser.parse_args()


    args.func(args)
