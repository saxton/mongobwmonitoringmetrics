import argparse
from pymongo import MongoClient
from pymongo import DESCENDING
from pymongo import ASCENDING
from pymongo.errors import OperationFailure
import pymongo
from datetime import datetime as dt
import random
import time
import numpy as np

'''
All available fields of a metric doc are pulled. Some of them we don't want in the final data cube.
And some of them we want for further processing but skip scaling. coord_keys, exclude_keys, and exclude_scale_keys
manage these caegorys. 
'''

database_name = 'monitoringData'
DATA_CUBE_DIM = 24
coord_keys = [
'nettopo_mesh_coord_X',
'nettopo_mesh_coord_Y',
'nettopo_mesh_coord_Z',
]

exclude_scale_keys = ['#Time'] + coord_keys
exclude_keys = ['_id', 'k_to_h', 'CompId', '#TimeTrunSec',
                'X+_recvlinkstatus (1)', 'X+_sendlinkstatus (1)',
                'X-_recvlinkstatus (1)', 'X-_sendlinkstatus (1)',
                'Y+_recvlinkstatus (1)', 'Y+_sendlinkstatus (1)',
                'Y-_recvlinkstatus (1)', 'Y-_sendlinkstatus (1)',
                'Z+_recvlinkstatus (1)', 'Z+_sendlinkstatus (1)',
                'Z-_recvlinkstatus (1)', 'Z-_sendlinkstatus (1)',
                'X+_SAMPLE_GEMINI_LINK_BW (B/s)',
                'X+_SAMPLE_GEMINI_LINK_CREDIT_STALL (% x1e6)',
                'X+_SAMPLE_GEMINI_LINK_INQ_STALL (% x1e6)',
                'X+_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE (B)',
                'X+_SAMPLE_GEMINI_LINK_USED_BW (% x1e6)',
                'X-_SAMPLE_GEMINI_LINK_BW (B/s)',
                'X-_SAMPLE_GEMINI_LINK_CREDIT_STALL (% x1e6)',
                'X-_SAMPLE_GEMINI_LINK_INQ_STALL (% x1e6)',
                'X-_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE (B)',
                'X-_SAMPLE_GEMINI_LINK_USED_BW (% x1e6)',
                'Y+_SAMPLE_GEMINI_LINK_BW (B/s)',
                'Y+_SAMPLE_GEMINI_LINK_CREDIT_STALL (% x1e6)',
                'Y+_SAMPLE_GEMINI_LINK_INQ_STALL (% x1e6)',
                'Y+_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE (B)',
                'Y+_SAMPLE_GEMINI_LINK_USED_BW (% x1e6)',
                'Y-_SAMPLE_GEMINI_LINK_BW (B/s)',
                'Y-_SAMPLE_GEMINI_LINK_CREDIT_STALL (% x1e6)',
                'Y-_SAMPLE_GEMINI_LINK_INQ_STALL (% x1e6)',
                'Y-_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE (B)',
                'Y-_SAMPLE_GEMINI_LINK_USED_BW (% x1e6)',
                'Z+_SAMPLE_GEMINI_LINK_BW (B/s)',
                'Z+_SAMPLE_GEMINI_LINK_CREDIT_STALL (% x1e6)',
                'Z+_SAMPLE_GEMINI_LINK_INQ_STALL (% x1e6)',
                'Z+_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE (B)',
                'Z+_SAMPLE_GEMINI_LINK_USED_BW (% x1e6)',
                'Z-_SAMPLE_GEMINI_LINK_BW (B/s)',
                'Z-_SAMPLE_GEMINI_LINK_CREDIT_STALL (% x1e6)',
                'Z-_SAMPLE_GEMINI_LINK_INQ_STALL (% x1e6)',
                'Z-_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE (B)',
                'Z-_SAMPLE_GEMINI_LINK_USED_BW (% x1e6)',
                'SAMPLE_bteout_optA (B/s)',
                'SAMPLE_bteout_optB (B/s)',
                'SAMPLE_fmaout (B/s)',
                'SAMPLE_totalinput (B/s)',
                'SAMPLE_totaloutput_optA (B/s)',
                'SAMPLE_totaloutput_optB (B/s)',
]

#exclude_keys.extend(coord_keys)

EXPECTED_NUM_KEY_METRICS=156

metric_fields = ['cTime', 'cTime_usec', 'DT', 'DT_usec', 'CompId',
       'nettopo_mesh_coord_X', 'nettopo_mesh_coord_Y', 'nettopo_mesh_coord_Z',
       'Xp_SAMPLE_GEMINI_LINK_BW', 'Xn_SAMPLE_GEMINI_LINK_BW',
       'Yp_SAMPLE_GEMINI_LINK_BW', 'Yn_SAMPLE_GEMINI_LINK_BW',
       'Zp_SAMPLE_GEMINI_LINK_BW', 'Zn_SAMPLE_GEMINI_LINK_BW',
       'Xp_SAMPLE_GEMINI_LINK_USED_BW', 'Xn_SAMPLE_GEMINI_LINK_USED_BW',
       'Yp_SAMPLE_GEMINI_LINK_USED_BW', 'Yn_SAMPLE_GEMINI_LINK_USED_BW',
       'Zp_SAMPLE_GEMINI_LINK_USED_BW', 'Zn_SAMPLE_GEMINI_LINK_USED_BW',
       'Xp_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE',
       'Xn_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE',
       'Yp_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE',
       'Yn_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE',
       'Zp_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE',
       'Zn_SAMPLE_GEMINI_LINK_PACKETSIZE_AVE',
       'Xp_SAMPLE_GEMINI_LINK_INQ_STALL', 'Xn_SAMPLE_GEMINI_LINK_INQ_STALL',
       'Yp_SAMPLE_GEMINI_LINK_INQ_STALL', 'Yn_SAMPLE_GEMINI_LINK_INQ_STALL',
       'Zp_SAMPLE_GEMINI_LINK_INQ_STALL', 'Zn_SAMPLE_GEMINI_LINK_INQ_STALL',
       'Xp_SAMPLE_GEMINI_LINK_CREDIT_STALL',
       'Xn_SAMPLE_GEMINI_LINK_CREDIT_STALL',
       'Yp_SAMPLE_GEMINI_LINK_CREDIT_STALL',
       'Yn_SAMPLE_GEMINI_LINK_CREDIT_STALL',
       'Zp_SAMPLE_GEMINI_LINK_CREDIT_STALL',
       'Zn_SAMPLE_GEMINI_LINK_CREDIT_STALL', 'SAMPLE_totaloutput_optA',
       'SAMPLE_totalinput', 'SAMPLE_fmaout', 'SAMPLE_bteout_optA',
       'SAMPLE_bteout_optB', 'SAMPLE_totaloutput_optB',
       'Rate_read_bytes_stats_snx11001', 'Rate_write_bytes_stats_snx11001',
       'Rate_open_stats_snx11001', 'Rate_close_stats_snx11001',
       'Rate_seek_stats_snx11001', 'Rate_read_bytes_stats_snx11002',
       'Rate_write_bytes_stats_snx11002', 'Rate_open_stats_snx11002',
       'Rate_close_stats_snx11002', 'Rate_seek_stats_snx11002',
       'Rate_read_bytes_stats_snx11003', 'Rate_write_bytes_stats_snx11003',
       'Rate_open_stats_snx11003', 'Rate_close_stats_snx11003',
       'Rate_seek_stats_snx11003', 'loadavg_latest', 'loadavg_5min',
       'loadavg_running_processes', 'loadavg_total_processes',
       'current_freemem', 'Rate_SMSG_ntx', 'Rate_SMSG_tx_bytes',
       'Rate_SMSG_nrx', 'Rate_SMSG_rx_bytes', 'Rate_RDMA_ntx',
       'Rate_RDMA_tx_bytes', 'Rate_RDMA_nrx', 'Rate_RDMA_rx_bytes',
       'Rate_ipogif0_rx_bytes', 'Rate_ipogif0_tx_bytes',
       'Tesla_K20X_gpu_util_rate', 'Tesla_K20X_gpu_memory_used',
       'Tesla_K20X_gpu_temp', 'Tesla_K20X_gpu_pstate',
       'Tesla_K20X_gpu_power_limit', 'Tesla_K20X_gpu_power_usage', 'Flag']


class emptyJobList(Exception):
    pass

class emptyMetricList(Exception):
    pass

class tooManyMetrics(Exception):
    pass

class poorlyFormedJobData(Exception):
    pass

class endOfJobMetricData(Exception):
    pass

def get_node_intervals(exec_host):
    wip = sorted([int(r.split('/')[0]) for r in exec_host.split('+')])
    chunks = []
    interval = [None, None]
    for w in wip:
        if interval[0] is None:
            interval[0] = w
            last = w
        else:
            if w > last + 1:
                interval[1] = last
                chunks.append(interval)
                interval = [None, None]
            else:
                last = w
    if interval[0] is not None:
        interval[1] = w
    chunks.append(interval)
    return chunks

def constelation_find_append(dbs_collection, query):
    for db_c in dbs_collection:
        for res in db_c.find(query):
            yield res

def constelation_find_min_max(dbs_collection, query, field):
    res_max = []
    res_min = []
    
    for db_c in dbs_collection:
        try:
            _n = db_c.find(query, {field:1}).sort(field, DESCENDING).limit(1).next()
        except StopIteration:
            pass
        else:
            res_max.append( _n[field])
        try:
            _n = db_c.find(query, {field:1}).sort(field, ASCENDING).limit(1).next()
        except StopIteration:
            pass
        else:
            res_min.append( _n[field])
    if len(res_max) == 0 or len(res_min) == 0:
        raise emptyMetricList("res_min {}, res_max {}".format(res_min, res_max))
    
    return min(res_min), max(res_max)


    #return [ res for db_c in dbs_collection for res in db_c.find(query)] #.batch_size(200000)

def constelation_find_estimated_document_count_sum(dbs_collection, query):
    #return sum([ db_c.find(query).estimated_document_count() for db_c in dbs_collection]) #.batch_size(200000)
    return sum([ db_c.find(query).count() for db_c in dbs_collection]) #.batch_size(200000)

def get_auto_nomalized_job_metrics(jobid, hosts_ports=None, dbs=None,
                                   delta_step=None, delta_index=None):
    _dbs = []
    if hosts_ports is not None:
        for host_port in hosts_ports:
            host, port = host_port.split(':')
            client = MongoClient(host, port, username="admin", password="admin")
            db = client.get_database(database_name)
            _dbs.append(db)
    elif dbs is not None:
        _dbs = dbs
    else:
        raise Exception('Must pass host, or db (collection).')

    torqueDataCollections = [db.torqueData for db in _dbs]
    metricDataCollections = [db.metricData for db in _dbs]
    len_resp_list = constelation_find_estimated_document_count_sum(torqueDataCollections, {"jobid": jobid, 'action': 'E'})
    resp_list = list(constelation_find_append(torqueDataCollections, {"jobid": jobid, 'action': 'E'}))

    if len_resp_list > 1 or len_resp_list == 0:
        actions = [r['action'] for r in resp_list]
        raise poorlyFormedJobData("Something is wrong, "
                                  "job {} data has multiple or 0 "
                                  "resluts from r['action']=='E'\n"
                                  "Only actions are {}".format(jobid, actions))

    if 'data' not in resp_list[0]:
        raise poorlyFormedJobData("Something is wrong, "
                                  "Job {} has no 'data' field. "
                                  "These are the fields it has {}".format(jobid, list(resp_list[0].keys())))

    missing_data_fields = [field_not_exist for field_not_exist in ['start', 'end', 'exec_host'] if field_not_exist not in resp_list[0]['data']]
    if len(missing_data_fields) > 0:
        raise poorlyFormedJobData("Something is wrong, "
                                  "Job {} has no {} fields. "
                                  "These are the fields it has {}".format(jobid,
                                                                          missing_data_fields,
                                                                          list(resp_list[0]['data'].keys())))
    
    start,end,exec_host = resp_list[0]['data']['start'], resp_list[0]['data']['end'], resp_list[0]['data']['exec_host']
    # delta_step=None, delta_index=None

    if delta_step is not None:
        _start = delta_step*delta_index + int(start)
        __end = delta_step*(delta_index + 1) + int(start)
        if _start >= int(end):
            raise endOfJobMetricData()
        _end = min(__end, int(end))
    else:
        _end = int(end)
        _start = int(start)

    node_inter = get_node_intervals(exec_host)                                                
    node_or_dict = {"$or": [{"CompId":{"$gte": ni[0], "$lte": ni[1]}} for ni in node_inter]}
    #time_or_dict = {"#TimeTrunSec": {"$gte": int(_start), "$lt": int(_end)}}
    time_or_dict = {"#Time": {"$gte": _start, "$lt": _end}}
    find_dict = {"$and":[time_or_dict, node_or_dict]}

    #est_count = constelation_find_estimated_document_count_sum(metricDataCollections, find_dict)
    #print("jobid {} has estimated doc count of {}. Starting find query".format(jobid, est_count))
    job_metricData = list(constelation_find_append(metricDataCollections, find_dict))

    job_metricData_keys = sorted(set([k for m in job_metricData for k in m.keys()]))
    job_metricData_keys = [k for k in job_metricData_keys if k not in exclude_keys]
    wip_min_max = {k: [float(m[k]) for m in job_metricData] for k in job_metricData_keys}
    min_max = {k: (min(v), max(v)) for k,v in wip_min_max.items()}
    def min_max_norm(m, k):
        delta = min_max[k][1] - min_max[k][0]
        if delta > 0:
            norm = (m[k] - min_max[k][0])/(delta)
        elif min_max[k][1] > 0:
            norm = 1.
        elif min_max[k][1] < 0:
            norm = -1
        else:
            norm = 0.0
        return norm
    job_metricData = [{k: min_max_norm(m, k) if k not in exclude_scale_keys else m[k]  for k in job_metricData_keys} \
                      for m in job_metricData]
    explain_dict = None
    return job_metricData, explain_dict


def get_job_metrics(jobid, hosts_ports=None, dbs=None,
                    delta_step=None, delta_index=None):
    _dbs = []
    if hosts_ports is not None:
        for host_port in hosts_ports:
            host, port = host_port.split(':')
            client = MongoClient(host, port, username="admin", password="admin")
            db = client.get_database(database_name)
            _dbs.append(db)
    elif dbs is not None:
        _dbs = dbs
    else:
        raise Exception('Must pass host, or db (collection).')

    torqueDataCollections = [db.torqueData for db in _dbs]
    metricDataCollections = [db.metricData for db in _dbs]
    
    resp_list = list(constelation_find_append(torqueDataCollections, {"jobid": jobid, 'action': 'E'}))

    if len(resp_list) > 1 or len(resp_list) == 0:
        actions = [r['action'] for r in resp_list]
        raise poorlyFormedJobData("Something is wrong, "
                                  "job {} data has multiple or 0 "
                                  "resluts from r['action']=='E'\n"
                                  "Only actions are {}".format(jobid, actions))

    if 'data' not in resp_list[0]:
        raise poorlyFormedJobData("Something is wrong, "
                                  "Job {} has no 'data' field. "
                                  "These are the fields it has {}".format(jobid, list(resp_list[0].keys())))

    missing_data_fields = [field_not_exist for field_not_exist in ['start', 'end', 'exec_host'] if field_not_exist not in resp_list[0]['data']]
    if len(missing_data_fields) > 0:
        raise poorlyFormedJobData("Something is wrong, "
                                  "Job {} has no {} fields. "
                                  "These are the fields it has {}".format(jobid,
                                                                          missing_data_fields,
                                                                          list(resp_list[0]['data'].keys())))
    
    start,end,exec_host = resp_list[0]['data']['start'], resp_list[0]['data']['end'], resp_list[0]['data']['exec_host']
    # delta_step=None, delta_index=None

    if delta_step is not None:
        _start = delta_step*delta_index + int(start)
        __end = delta_step*(delta_index + 1) + int(start)
        if _start >= int(end):
            raise endOfJobMetricData()
        _end = min(__end, int(end))
    else:
        _end = int(end)
        _start = int(start)

    node_inter = get_node_intervals(exec_host)                                                
    node_or_dict = {"$or": [{"CompId":{"$gte": ni[0], "$lte": ni[1]}} for ni in node_inter]}
    #time_or_dict = {"#TimeTrunSec": {"$gte": int(_start), "$lt": int(_end)}}
    time_or_dict = {"#Time": {"$gte": _start, "$lt": _end}}
    find_dict = {"$and":[time_or_dict, node_or_dict]}

    #est_count = constelation_find_estimated_document_count_sum(metricDataCollections, find_dict)
    #print("jobid {} has estimated doc count of {}. Starting find query".format(jobid, est_count))
    t_min, t_max = constelation_find_min_max(metricDataCollections, find_dict, '#Time')
    job_metricData_generator = constelation_find_append(metricDataCollections, find_dict)
    #job_metricData = [{k: m[k] for k in m if k not in exclude_scale_keys} \
    #                  for m in job_metricData_generator]
    return job_metricData_generator, t_min, t_max


def get_agrigate_job_metrics(jobid, metric_name, host=None, db=None):
    """
    DEFUNCT. Must re-write
    """
    if host is not None:
        client = MongoClient(host, 27019, username="admin", password="admin")
        db = client.get_database(database_name)
        torqueData = db.torqueData
    elif db is not None:
        torqueData = db.torqueData
    else:
        raise Exception('Must pass host or db (collection).')

    resp = torqueData.find({"jobid": jobid})

    resp_list = [r for r in resp if r['action']=='E']
    if len(resp_list) > 1 or len(resp_list) == 0:
        actions = [r['action'] for r in resp_list]
        raise poorlyFormedJobData("Something is wrong, "
                                  "job {} data has multiple or 0 "
                                  "resluts from r['action']=='E'\n"
                                  "Only actions are {}".format(jobid, actions))

    if 'data' not in resp_list[0]:
        raise poorlyFormedJobData("Something is wrong, "
                                  "Job {} has no 'data' field. "
                                  "These are the fields it has {}".format(jobid, list(resp_list[0].keys())))

    missing_data_fields = [field_not_exist for field_not_exist in ['start', 'end', 'exec_host'] if field_not_exist not in resp_list[0]['data']]
    if len(missing_data_fields) > 0:
        raise poorlyFormedJobData("Something is wrong, "
                                  "Job {} has no {} fields. "
                                  "These are the fields it has {}".format(jobid,
                                                                          missing_data_fields,
                                                                          list(resp_list[0]['data'].keys())))
    
    start,end,exec_host = resp_list[0]['data']['start'], resp_list[0]['data']['end'], resp_list[0]['data']['exec_host']

    
    node_inter = get_node_intervals(exec_host)                                                
    node_or_dict = {"$or": [{"CompId":{"$gte": ni[0], "$lte": ni[1]}} for ni in node_inter]}
    #time_or_dict = {"#TimeTrunSec": {"$gte": int(start), "$lt": int(end)}}
    time_or_dict = {"#Time": {"$gte": int(start), "$lt": int(end)}}
    find_dict = {"$and":[time_or_dict, node_or_dict]}
    metricData_Curs = db.metricData.find(find_dict).batch_size(200000) # .hint( "CompId_1_#TimeTrunSec_1" )
    #metricData_Curs = db.metricData.find(find_dict).hint([( "$natural" , 1 )])
    
    job_metricData = list(metricData_Curs)
    explain_dict = None #metricData_Curs.explain()
    job_metricData_keys = sorted(set([k for m in job_metricData for k in m.keys()]))
    job_metricData_keys = [k for k in job_metricData_keys if k not in exclude_keys]
    wip_min_max = {k: [float(m[k]) for m in job_metricData] for k in job_metricData_keys}
    min_max = {k: (min(v), max(v)) for k,v in wip_min_max.items()}
    def min_max_norm(m, k):
        delta = min_max[k][1] - min_max[k][0]
        if delta > 0:
            norm = (m[k] - min_max[k][0])/(delta)
        elif min_max[k][1] > 0:
            norm = 1.
        elif min_max[k][1] < 0:
            norm = -1
        else:
            norm = 0.0
        return norm
    job_metricData = [{k: min_max_norm(m, k) if k not in exclude_scale_keys else m[k]  for k in job_metricData_keys} \
                      for m in job_metricData]
    return job_metricData, explain_dict


def jobs_strata_metrics(start_d, end_d, min_num_nodes, max_num_nodes,
                        min_duration, max_duration,
                        limit, hosts_ports=None, dbs=None, index=0, num_workers=1,
                        shuffle=False, shuffle_seed=1, offset=None, delta_step=None):
    """
    Generator that yields jobid, jobs_data, explain_dict.

    Args:
        start_d (datetime): jobs newer than this date
        end_d (datetime): jobs older than this date
        min_num_nodes (int): jobs with this many or more nodes.
        max_num_nodes (int): jobs with this many or less nodes.
        min_duration (float): min
        max_duration (float): min
        limit (int): limits the number of jobs returned. If None type, no limit.
        hosts_ports (list): list of string with host:port
        index (int): rank
        num_workers (int): total number of ranks.
        shuffle (boot): shuffles list of metrics.
        shuffle_seed (int): used to force same shuffle accross multiple ranks
    """
    _dbs=[]
    if hosts_ports is not None:
        for h_p in hosts_ports:
            host, port = h_p.split(':')
            client = MongoClient(host, port, username="admin", password="admin")
            _dbs.append( client.get_database(database_name))
    elif dbs is not None:
        _dbs=dbs
    else:
        raise Exception('Must pass host or dbs (collection).')
    
    jobs_nc_strata = jobs_strata(start_d, end_d, min_num_nodes, max_num_nodes,
                                 min_duration, max_duration,
                                 limit, hosts_ports=hosts_ports, dbs=dbs, offset=offset)

    wip_nc_jobid_list = [(nc, job['jobid']) for nc in sorted(jobs_nc_strata.keys()) for job in jobs_nc_strata[nc]]
    nc_jobid_list = wip_nc_jobid_list[index::num_workers]
    total_possible_jobs = len(wip_nc_jobid_list)
    print('total_possible_jobs {}'.format(total_possible_jobs))
    """
    if shuffle:
        shuffled_index = list(range(total_possible_jobs))
        random.shuffle(shuffled_index)
        nc_jobid_list = [wip_nc_jobid_list[i] for i in shuffled_index]
    else:
        nc_jobid_list = wip_nc_jobid_list
    """ 
    if shuffle:
        random.seed(shuffle_seed)
        random.shuffle(nc_jobid_list)

    for nc, jobid in nc_jobid_list:
        delta_index = 0
        while True:
            tick = time.time()
            try:
                try:
                    jobs_data_generator, t_min, t_max = get_job_metrics(jobid, dbs=dbs, delta_step=delta_step,
                                                                        delta_index=delta_index)
                except endOfJobMetricData:
                    break

            except (poorlyFormedJobData, OperationFailure, emptyMetricList) as e:
                if type(e) == OperationFailure:
                    print('get_job_metrics raised Operations Failure for jobid {}. skipping. Message: {}'.format(jobid, str(e)))
                elif type(e) == poorlyFormedJobData:
                    print('in jobs_strata_metrics, poorlyFormedJobData for jobid {}. skipping. Message: {}'.format(jobid, str(e)))
                elif type(e) == emptyMetricList:
                    print('in jobs_strata_metrics, emptyMetricList for jobid {}. skipping. Message: {}'.format(jobid, str(e)))
                break

            tock = time.time()
            print(
                "num_node: {}, "
                "jobid: {}, "
                "query time (sec): {:.3f}, "
                "delta_step: {}, "
                "delta_index: {}".format(nc, jobid, (tock-tick),
                                         delta_step, delta_index)
            )
            delta_index += 1
            yield jobid, jobs_data_generator, t_min, t_max
            

def jobs_strata(start_d, end_d, min_num_nodes, max_num_nodes,
                min_duration, max_duration, limit, hosts_ports=None, dbs=None, offset=None):
    """
    returns a dictionary with number of nodes as keys
    and values of torque log entries (action E).

    Args:
        start_d (datetime): jobs newer than this date
        end_d (datetime): jobs older than this date
        min_num_nodes (int): jobs with this many or more nodes.
        max_num_nodes (int): jobs with this many or less nodes.
        min_duration (float): min
        max_duration (float): min
        limit (int): limits the number of jobs returned. If None type, no limit.
        offset (int): in the list of jobids returned, skip the first offeset number of them
        host (str): mongodb routher host
        db (pymongo obj): pymongo database object
    """
    print("Looking up jobs strata start_d {}, end_d {}, min_num_nodes {}, max_num_nodes {}, min_duration {}, max_duration {}, limit {}".format(
    start_d, end_d, min_num_nodes, max_num_nodes,min_duration, max_duration, limit))
    _dbs=[]
    
    if hosts_ports is not None:
        for host_port in hosts_ports:
            host, port = host_port.split(':')
            port = int(port)
            client = MongoClient(host, port, username="admin", password="admin")
            db = client.get_database(database_name)
            _dbs.append(db)
    elif dbs is not None:
        _dbs=dbs
    else:
        raise Exception('Must pass host or db (collection).')

    torqueDataCollections = [db.torqueData for db in _dbs]
    
    """
    cur =  torqueData.find(
        {'dateTime': {"$gt": start_d, "$lt": end_d }, 'action': "E"}
    ).sort(
        [('dateTime', pymongo.ASCENDING)]
    )
    """
    avail_jobs_data_ended = constelation_find_append(
        torqueDataCollections,
        {
            'dateTime': {"$gt": start_d, "$lt": end_d },
            'action': "E",
            'data.Exit_status': '0',
        }
    )
    jobs_nc_strata = {nc: list() for nc in range(min_num_nodes, max_num_nodes+1)}
    for a in avail_jobs_data_ended:
        if (int(a['data']['end'])  - int(a['data']['start']))/60. < max_duration \
           and (int(a['data']['end'])  - int(a['data']['start']))/60. > min_duration \
           and int(a['data']['unique_node_count']) >= min_num_nodes \
           and int(a['data']['unique_node_count']) <= max_num_nodes:
            jobs_nc_strata[int(a['data']['unique_node_count'])].append(a)

    _zero_len_strata = [(nc, jobs_nc_strata.pop(nc)) for nc in range(min_num_nodes, max_num_nodes+1) if len(jobs_nc_strata[nc]) == 0 ]
        
    if limit is not None:
        for nc, a in jobs_nc_strata.items():
            num_jobs = len(a)
            if num_jobs > limit:
                lower = offset if offset is not None else random.choice(range(num_jobs - limit))
                upper = lower + limit
            else:
                lower = 0
                upper = num_jobs
            jobs_nc_strata[nc] = a[lower:upper]
                
    return jobs_nc_strata


def jobs_from_group_metrics(group, delta_step=None, hosts_ports=None, dbs=None,
                            index=0, num_workers=1, limit=None, offset=None):
    avail_jobs_from_group = jobs_from_group(group, hosts_ports, dbs, limit, offset)

    for job_count, job in enumerate(avail_jobs_from_group):
        if job_count % num_workers != index:
            continue
        jobid = job['jobid']
        nc = int(job['data']['unique_node_count'])
        delta_index = 0
        while True:
            tick = time.time()
            try:
                try:
                    jobs_data_generator, t_min, t_max = get_job_metrics(jobid, dbs=dbs, delta_step=delta_step,
                                                                        delta_index=delta_index)
                except endOfJobMetricData:
                    break

            except (poorlyFormedJobData, OperationFailure, emptyMetricList) as e:
                if type(e) == OperationFailure:
                    print('get_job_metrics raised Operations Failure for jobid {}. skipping. Message: {}'.format(jobid, str(e)))
                elif type(e) == poorlyFormedJobData:
                    print('in jobs_strata_metrics, poorlyFormedJobData for jobid {}. skipping. Message: {}'.format(jobid, str(e)))
                elif type(e) == emptyMetricList:
                    print('in jobs_strata_metrics, emptyMetricList for jobid {}. skipping. Message: {}'.format(jobid, str(e)))
                break

            tock = time.time()
            print(
                "num_node: {}, "
                "jobid: {}, "
                "query time (sec): {:.3f}, "
                "delta_step: {}, "
                "delta_index: {}".format(nc, jobid, (tock-tick),
                                         delta_step, delta_index)
            )
            delta_index += 1
            yield jobid, jobs_data_generator, t_min, t_max


def jobs_from_group(group, hosts_ports=None, dbs=None, limit=None, offset=None):
    if hosts_ports is not None:
        for host_port in hosts_ports:
            host, port = host_port.split(':')
            port = int(port)
            client = MongoClient(host, port, username="admin", password="admin")
            db = client.get_database(database_name)
            _dbs.append(db)
    elif dbs is not None:
        _dbs=dbs
    else:
        raise Exception('Must pass host or db (collection).')

    torqueDataCollections = [db.torqueData for db in _dbs]
    query = {
        'action': "E",
        'data.Exit_status': '0',
        'data.account' : group
    }
    len_resp_list = constelation_find_estimated_document_count_sum(torqueDataCollections, query)
    print("{} jobs in group {}".format(len_resp_list,group))
    avail_jobs_from_group = constelation_find_append(
        torqueDataCollections,
        query
    )

    return avail_jobs_from_group

def make_job_cube(data_from_mongo,  data_cube_t_length=60,
                  feature_major_order=False, key_length=None,
                  t_min=None, t_max=None, jobid=None, normalize=False):
    '''
    :int data_cube_t_length: data_cube_t_length can be None to return actual data length
    :int key_length: if key_length is not None, this func will return a job cube of all 0's
    '''
    import pdb;
    if data_from_mongo is not None:
        i_data_from_mongo = next(data_from_mongo)

    if key_length is not None:
        _key_length = key_length
    else:
        keys = sorted(list(set([k for k in i_data_from_mongo.keys() if k not in exclude_keys + exclude_scale_keys])))
        #keys = sorted(list(set([k for d in data_from_mongo for k in d.keys() if k not in metric_fields])))
        _key_length = len(keys)
        keys_id = list(range(_key_length))

    if feature_major_order:
        pass
    else:
        if data_cube_t_length is not None:
            data_shape = [data_cube_t_length] + 3*[DATA_CUBE_DIM] + [_key_length]
            clip_t_index = data_cube_t_length
        else:
            data_shape = [num_min] + 3*[DATA_CUBE_DIM] + [_key_length]
            clip_t_index = num_min

        data_t_cube = np.zeros(data_shape, dtype=np.float32)
        if data_from_mongo is not None and data_cube_t_length is not None:
            num_min = int(( int(t_max) - int(t_min))/60.) + 1
            def _rewind_data_from_mongo():
                yield i_data_from_mongo
                for dm in data_from_mongo:
                    yield dm

            for doc_count, dm in enumerate(_rewind_data_from_mongo()):
                i = int((int(dm['#Time']) - t_min)/60)
                if i >= clip_t_index:
                    continue
                x,y,z = [dm[k] for k in coord_keys]
                ordered_values = [float(dm[k]) for k in keys]
                np.put(data_t_cube[i,x,y,z], keys_id, ordered_values)
                    
            print('make_data_cube jobid: {}, doc_count: {}'.format(jobid, doc_count))

    # normalize
    if normalize:
        d_cube_min = np.min(data_t_cube, axis=0)
        d_cube_scale = np.max(data_t_cube, axis=0) - d_cube_min
        data_t_cube = (data_t_cube - d_cube_min)/d_cube_scale
        np.nan_to_num(data_t_cube, copy=False)
    
    return data_t_cube

def get_constelation_hosts(host=None, db=None):
    if host is not None:
        client = MongoClient(host, 27019, username="admin", password="admin")
        db = client.get_database('constelation')
        
    elif db is not None:
        pass
    else:
        raise Exception('Must pass host or db (collection).')

    conf_files=list(db.routerConstelation.distinct("conf_file"))    
    constelation_dict = {l: None for l in conf_files}
    for k in constelation_dict:
        _raw_host_list = list(db.routerConstelation.find({"conf_file": k}, {"host": 1, "port": 1} ))
        for v in _raw_host_list:
            del(v['_id'])
        constelation_dict[k] = _raw_host_list
    return constelation_dict


if __name__== "__main__":
    def valid_date(s):
        try:
            return dt.strptime(s, "%Y%m%d")
        except ValueError:
            msg = "Not a valid date: '{0}'.".format(s)
            raise argparse.ArgumentTypeError(msg)

    parser = argparse.ArgumentParser(description='ingest config args')

    parser.add_argument('host', type=str,
                        help='router host')
    parser.add_argument('jobid', type=str,
                        help='job ID')
    parser.add_argument('--dateRange', type=valid_date, nargs=2,
                        help='dates in format YYYYMMDD (e.g. 20181119). '
                        'First date must be smaller that second. '
                        'Dates will be denerated in as the half '
                        'open interval, e.g. [0,1). ')

    args = parser.parse_args()
    get_auto_nomalized_job_metrics(args.jobid, host=args.host)
    

