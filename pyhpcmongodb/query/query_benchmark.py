import get
import random

import argparse
from pymongo import MongoClient
from datetime import datetime as dt

database_name = 'monitoringData'


def main(start_d, end_d,  min_duration, max_duration, num_sample,hosts):

    for index in range(num_sample):
        if index % 5 == 0:
            host = random.choice(hosts)
        n_n = random.choice([4,8,10, 16,20, 64,128, 200, 512, 1000, 1024])
        for i in range(100):
            try:
                get.jobs_strata(start_d, end_d, n_n, min_duration, max_duration, limit=5, host=host)
            except get.emptyJobList:
                pass
            else:
                break

    


if __name__== "__main__":
    def valid_date(s):
        try:
            return dt.strptime(s, "%Y%m%d")
        except ValueError:
            msg = "Not a valid date: '{0}'.".format(s)
            raise argparse.ArgumentTypeError(msg)
    parser = argparse.ArgumentParser(description='ingest config args')

    parser.add_argument('hosts', type=str, nargs='+',
                        help='list of router hosts')
    parser.add_argument('--dateRange', type=valid_date, nargs=2,
                        help='dates in format YYYYMMDD (e.g. 20181119). '
                        'First date must be smaller that second. '
                        'Dates will be denerated in as the half '

                        'open interval, e.g. [0,1). ')

    parser.add_argument('--minMaxDurration', type=int, nargs=2,
                        help='min and max in hours')

    parser.add_argument('--numSamples', type=int,
                        help='min and max in hours')

    args = parser.parse_args()

    main(args.dateRange[0],
         args.dateRange[1],
         args.minMaxDurration[0],
         args.minMaxDurration[1],
         args.numSamples,
         hosts=args.hosts)
